/*
 * Name: Brody Paulk
 * Assignment: Lab 6
 * Section: Section 4
 */
#include <iostream>
#include "string"
using namespace std;

struct ZipCode {
   public: // define the members of this struct

    ZipCode fromcode(int code); // constructor 1, to take a 5 digit zipcode as an int, example 32901

    ZipCode frombarcode(string barcode); // constructor 2, to take a 5 digit zipcode as string with 27 characters (1 and 0)

    // implement
    int getZipCode();

    // implement
    string getBarcode();

    // private functions can only be called from other functions inside the struct, they cannot be used outside
private:
    ZipCode(string, int);
    string barcode;
    int zipcode;
    bool checkBarCode(string);

    // encodes a integer zipcode to a barcode
    string encode(int);
    // decodes a barcode to a zipcode
    int decode(string);

    bool checksum() {
        // this is the extra credit portion
        return false;
    }
};

ZipCode::ZipCode(string barcode, int code) : barcode(barcode), zipcode(code){}

ZipCode ZipCode::fromcode(int code) {
    string barcode = encode(code);
    if (checkBarCode(barcode)) {
        return ZipCode(barcode, code);
    }
    else{ cout<< "invalid zipcode";}
}

ZipCode ZipCode::frombarcode(string barcode){
    if(checkBarCode(barcode)){
        return ZipCode(barcode, decode(barcode));
    }
    else{cout<< "invalid barcode";}
}

string ZipCode::getBarcode() {
    return barcode;
}

int ZipCode::getZipCode(){
    return zipcode;
}

bool ZipCode::checkBarCode(string barcode) {
    //lenth must be 27
    if (barcode.length() != 27) {
        cout << "invalid bar code";
        return false;
    }
 const int groupeddigits = 5; //digits in split barcode

 //check for exactly 2 ones in each group
 for (int count = 1; count < barcode.length()-1; count+=groupeddigits){
    int count_ones = 0;
    for (int i = 0; i < groupeddigits; i++){
        if(barcode[count+i] == '1') { 
            ++count_ones;
        }
    if (count_ones != 2){
        cout << "invalid barcode";
    }
    }
return true;
}
}

string ZipCode::encode(int code) {
    int groupeddigits = 5;
    int encodearray[] = {7, 4, 2, 1, 0};

    string barcode;

    int codedigit = 0;
    while (code > 0) {
        string group = "00000";
        codedigit = code % 10;
        code = code / 10;
        for (int i = 0; i < groupeddigits; i++) {
            for (int j = i +1; j < groupeddigits; j++) {
                if (codedigit == 0 && encodearray[i]+encodearray[j] == 11) {
                    group[i] = '1';
                    group[j] = '1';
                    barcode.insert(0,group);
                }
                if (codedigit == encodearray[i] + encodearray[j] ){
                    group[i] = '1';
                    group[j] = '1';
                    barcode.insert(0,group);
                }
            }
        }
    }
    barcode.insert(0,"1");
    barcode.insert(barcode.length(), "1");

    return barcode;
}

int ZipCode::decode(string barcode) {
    int groupeddigits = 5;
   const short decodearray[] = { 7, 4, 2, 1, 0 };

   string strcode;
   int count = 1;
   while (count < (barcode.length() - 1))
   {
      int multiple = 0;
      for (int i = 0; i < groupeddigits; i++)
      {
         multiple += (barcode[count++] - 48) * decodearray[i];

         if (multiple == 11) multiple = 0;
      }
      strcode += to_string(multiple);
   }
   return atoi(strcode.c_str());
}

int stringNotEquals(const string& s1, const string& s2) {
    return s1.compare(s2);
}


int main(int argc, char *argv[]) {

 


    // write code to ask user to enter zip code or barcode
    int barcode;
    int code = 32901;
    cout << barcode;
    
    return 0;
}
